Config = {
    "gman": {"url": "http://172.17.0.1:8089"},
    "storage": {
        "url": "172.17.0.1:9000",
        "access_key_secret": "/var/openfaas/secrets/access-key",
        "secret_key_secret": "/var/openfaas/secrets/secret-key",
    },
    "name": "fortune_gateway",
    "executor_url": "http://172.17.0.1:8080/async-function/piperci-"
    + "fortune-executor",
    "type": "gateway",
    "supported_commands_file": "./function/supported_commands",
}

try:
    with open(Config["storage"]["access_key_secret"], "r") as access_key_file:
        Config["storage"]["access_key"] = access_key_file.readline().strip("\n")

    with open(Config["storage"]["secret_key_secret"], "r") as access_key_file:
        Config["storage"]["secret_key"] = access_key_file.readline().strip("\n")
except (KeyError, IOError):
    Config["storage"]["access_key"] = ""
    Config["storage"]["secret_key"] = ""


try:
    with open(Config["supported_commands_file"], "r") as supported_commands_file:
        Config["supported_commands"] = list(
            map(lambda line: line.strip("\n"), supported_commands_file.readlines())
        )
except (KeyError, IOError):
    Config["supported_commands"] = list()
