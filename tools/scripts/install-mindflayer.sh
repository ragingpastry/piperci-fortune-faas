
INSTALL_TARGET=$1
TMPDIR=$2

curl -o ${TMPDIR}/piperci-mindflayer-master.tar.gz https://gitlab.com/dreamer-labs/piperci/piperci-mindflayer/-/archive/master/piperci-mindflayer-master.tar.gz

tar -C ${TMPDIR} -xzf ${TMPDIR}/piperci-mindflayer-master.tar.gz

cp -r ${TMPDIR}/piperci-mindflayer-master/tests ${INSTALL_TARGET}/
